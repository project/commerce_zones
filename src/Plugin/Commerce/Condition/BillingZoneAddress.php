<?php

namespace Drupal\commerce_zones\Plugin\Commerce\Condition;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the commerce zone address condition.
 *
 * @CommerceCondition(
 *   id = "billing_address_zone",
 *   label = @Translation("Billing address zone"),
 *   category = @Translation("Customer"),
 *   entity_type = "commerce_order",
 *   weight = 10,
 * )
 */
class BillingZoneAddress extends BaseZoneAddress {

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $billing_profile = $order->getBillingProfile();
    if (!$billing_profile) {
      return FALSE;
    }
    $address = $billing_profile->get('address')->first();
    if (!$address) {
      // The conditions can't be applied until the shipping address is known.
      return FALSE;
    }

    return $this->evaluateAddress($address, $this->configuration['zones']);
  }

}
