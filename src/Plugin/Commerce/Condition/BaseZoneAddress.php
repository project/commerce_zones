<?php

namespace Drupal\commerce_zones\Plugin\Commerce\Condition;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base class for commerce conditions.
 */
abstract class BaseZoneAddress extends ConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The entity UUID mapper.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The zone storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $zoneStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->zoneStorage = $entity_type_manager->getStorage('commerce_zone');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'negate' => FALSE,
      'zones' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Map the UUIDs back to IDs for the form element.
    $zone_ids = $this->configuration['zones'];
    $form['zones'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Commerce zones'),
      '#default_value' => $zone_ids,
      '#target_type' => 'commerce_zone',
      '#hide_single_entity' => FALSE,
      '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    $form['negate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negate'),
      '#description' => $this->t('If checked, the value(s) selected should not match.'),
      '#default_value' => $this->configuration['negate'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['zones'] = $values['zones'];
    $this->configuration['negate'] = $values['negate'];
  }

  /**
   * Helper to evaluate match of address against multiple zones.
   *
   * @param \Drupal\address\Plugin\Field\FieldType\AddressItem $address
   *   The address field values.
   * @param array $zone_ids
   *   The Commerce zone ids.
   *
   * @return bool
   *   Return true on match or negate.
   */
  protected function evaluateAddress(AddressItem $address, array $zone_ids) {

    if (empty($this->configuration['zones'])) {
      return TRUE;
    }

    $negate_condition = (bool) $this->configuration['negate'];

    /** @var \Drupal\commerce_zones\Entity\ZoneInterface[] $commerce_zones */
    $commerce_zones = $this->zoneStorage->loadMultiple($zone_ids);

    foreach ($commerce_zones as $commerce_zone) {
      $match = $commerce_zone->match($address);

      // Early return if address got match.
      // If condition is negated, return FALSE.
      // If condition is not negated, return TRUE.
      if ($match) {
        return !$negate_condition;
      }
    }

    // If we reached here means there is no single match made.
    // For negated conditions returns TRUE, for non-negated return FALSE.
    return $negate_condition;
  }

}
