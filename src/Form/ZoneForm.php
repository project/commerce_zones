<?php

namespace Drupal\commerce_zones\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for Zone entities.
 */
class ZoneForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\commerce_zones\Entity\ZoneInterface $commerce_zone */
    $commerce_zone = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $commerce_zone->label(),
      '#description' => $this->t("Label for the Commerce Zone."),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $commerce_zone->getDescription(),
      '#description' => $this->t("Description for the Commerce Zone."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $commerce_zone->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_zones\Entity\Zone::load',
      ],
      '#disabled' => !$commerce_zone->isNew(),
    ];

    $form['configuration'] = [
      '#type' => 'address_zone',
      '#default_value' => $commerce_zone->getConfiguration(),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#options' => [
        0 => $this->t('Disabled'),
        1  => $this->t('Enabled'),
      ],
      '#default_value' => (int) $commerce_zone->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\commerce_zones\Entity\ZoneInterface $commerce_zone */
    $commerce_zone = $this->entity;
    $commerce_zone->setConfiguration($form_state->getValue(['configuration']));
    $commerce_zone->setDescription($form_state->getValue(['description']));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $commerce_zone = $this->entity;
    $status = $commerce_zone->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Commerce Zone.', [
          '%label' => $commerce_zone->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Commerce Zone.', [
          '%label' => $commerce_zone->label(),
        ]));
    }
    $form_state->setRedirectUrl($commerce_zone->toUrl('collection'));
  }

}
