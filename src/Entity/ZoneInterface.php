<?php

namespace Drupal\commerce_zones\Entity;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Commerce Zone entities.
 */
interface ZoneInterface extends ConfigEntityInterface {

  /**
   * Gets the zone configuration.
   *
   * @return array
   *   The payment gateway plugin configuration.
   */
  public function getConfiguration();

  /**
   * Sets the zone configuration.
   *
   * @param array $configuration
   *   The zones data.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration);

  /**
   * Gets the zone description.
   *
   * @return string
   *   The zone description.
   */
  public function getDescription();

  /**
   * Sets the zone description.
   *
   * @param string $description
   *   The zone description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the zone configuration.
   *
   * @return \CommerceGuys\Addressing\Zone\Zone
   *   The formatted zone.
   */
  public function getZone();

  /**
   * Matching address with a zone.
   *
   * @param \Drupal\address\Plugin\Field\FieldType\AddressItem $address
   *   The address item.
   *
   * @return bool
   *   Return match.
   */
  public function match(AddressItem $address);

}
