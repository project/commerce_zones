<?php

namespace Drupal\commerce_zones\Entity;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use CommerceGuys\Addressing\Zone\Zone as CoreZone;

/**
 * Defines the Commerce Zone entity.
 *
 * @ConfigEntityType(
 *   id = "commerce_zone",
 *   label = @Translation("Commerce Zone"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_zones\ZoneListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_zones\Form\ZoneForm",
 *       "edit" = "Drupal\commerce_zones\Form\ZoneForm",
 *       "delete" = "Drupal\commerce_zones\Form\ZoneDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "commerce_zone",
 *   admin_permission = "administer commerce_zone",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "configuration",
 *     "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/config/commerce-zones/add",
 *     "edit-form" = "/admin/commerce/config/commerce-zones/{commerce_zone}/edit",
 *     "delete-form" = "/admin/commerce/config/commerce-zones/{commerce_zone}/delete",
 *     "collection" = "/admin/commerce/config/commerce-zones"
 *   }
 * )
 */
class Zone extends ConfigEntityBase implements ZoneInterface {

  /**
   * The Commerce Zone ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Commerce Zone label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Commerce Zone description.
   *
   * @var string
   */
  protected $description;

  /**
   * The address zones.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getZone() {
    return new CoreZone([
      'id' => $this->id,
      'label' => $this->label,
    ] + $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function match(AddressItem $address) {
    return $this->getZone()->match($address);
  }

}
