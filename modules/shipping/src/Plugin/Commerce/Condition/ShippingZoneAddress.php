<?php

namespace Drupal\commerce_zones_shipping\Plugin\Commerce\Condition;

use Drupal\commerce_zones\Plugin\Commerce\Condition\BaseZoneAddress;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the commerce zone address condition.
 *
 * @CommerceCondition(
 *   id = "shipping_address_zone",
 *   label = @Translation("Shipping address zone"),
 *   category = @Translation("Customer"),
 *   entity_type = "commerce_shipment",
 *   weight = 10,
 * )
 */
class ShippingZoneAddress extends BaseZoneAddress {

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $entity;
    $shipping_profile = $shipment->getShippingProfile();
    if (!$shipping_profile) {
      return FALSE;
    }
    $address = $shipping_profile->get('address')->first();
    if (!$address) {
      // The conditions can't be applied until the shipping address is known.
      return FALSE;
    }
    return $this->evaluateAddress($address, $this->configuration['zones']);
  }

}
