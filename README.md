CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers

INTRODUCTION
------------

The Commerce Zones module enables you to create territorial groupings based on
countries, subdivisions, postal codes. The module comes out of box with
Commerce 2 condition plugin for shipping and billing address.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_zones

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/commerce_zones

REQUIREMENTS
------------

This module requires the following modules:

* [Commerce](https://www.drupal.org/project/commerce)

RECOMMENDED MODULES
-------------------

* None

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:

  - Administer zones

    Users with this permission will have access to all zones and menu item
    in Commerce.

* Go to /admin/commerce/config/commerce-zones/ for administering existing or
  adding new zones.

* Optional: enable shipping and example submodules


TROUBLESHOOTING
---------------

* Please open an issue at https://www.drupal.org/project/issues/commerce_zones

FAQ
---

* Please open an issue at https://www.drupal.org/project/issues/commerce_zones


MAINTAINERS
-----------

Current maintainers:
* Valentino Medimorec (valic) - https://www.drupal.org/u/valic
