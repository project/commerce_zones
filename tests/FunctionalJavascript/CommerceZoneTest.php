<?php

namespace Drupal\Tests\commerce_zones\FunctionalJavascript;

use Drupal\commerce_zones\Entity\Zone;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Tests the commerce zones UI.
 *
 * @group commerce_zones
 */
class CommerceZoneTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_zones',
    'commerce_zones_example',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_zone',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Tests adding a zone.
   */
  public function testCommerceZoneCreation() {
    $this->drupalGet('admin/commerce/config/commerce-zones');
    $this->getSession()->getPage()->clickLink('Add Zone');
    $this->assertSession()->pageTextContains(t('Territory'));
    $this->assertSession()->pageTextContains(t('Afghanistan'));
    $this->getSession()->getPage()->fillField('label', 'Test');
    $this->getSession()->getPage()->fillField('description', 'Test');
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextContains(t('Created the @label Commerce Zone.', ['@label' => 'Test']));

    /** @var \Drupal\commerce_zones\Entity\ZoneInterface $zone */
    $zone = Zone::load('test');

    $this->assertEquals('test', $zone->id());
    $this->assertEquals('Test', $zone->label());

    $territories = $zone->getZone()->getTerritories();
    foreach ($territories as $territory) {
      $this->assertEquals($territory->getCountryCode(), 'AF');
    }
  }

  /**
   * Tests adding a zone.
   */
  public function testCommerceZoneEdit() {
    $this->drupalGet('admin/commerce/config/commerce-zones/osijek_county/edit');
    $this->assertSession()->pageTextContains(t('Territory'));
    $this->assertSession()->pageTextContains(t('Croatia'));
    $this->assertSession()->pageTextContains(t('Included postal codes'));
    $this->assertSession()->pageTextContains(t('/(31)[0-9]{3}/'));
  }

}
