<?php

namespace Drupal\Tests\commerce_zones\Kernel;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\commerce_zones\Entity\Zone;
use Drupal\commerce_zones\Entity\ZoneInterface;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests zones conditions.
 *
 * @coversDefaultClass \Drupal\commerce_zones\Plugin\Commerce\Condition\BaseZoneAddress
 * @group commerce_zones
 */
class ZonesConditionTest extends OrderKernelTestBase {

  /**
   * The test order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The test profile.
   *
   * @var \Drupal\profile\Entity\Profile
   */
  protected $profile;

  /**
   * The test promotions.
   *
   * @var \Drupal\commerce_promotion\Entity\Promotion[]
   */
  protected $promotions;

  /**
   * The test zones.
   *
   * @var \Drupal\commerce_zones\Entity\Zone[]
   */
  protected $zones;

  /**
   * Test zones without HR country code.
   *
   * @var string[]
   */
  protected $nonHrZone = ['safta', 'efta', 'midwest_usa'];

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_zones',
    'commerce_zones_example',
    'commerce_promotion',
    'commerce_promotion_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_zone');
    $this->installEntitySchema('commerce_promotion');
    $this->installConfig(
      ['commerce_promotion', 'commerce_zones', 'commerce_zones_example']
    );
    $this->installSchema('commerce_promotion',
      ['commerce_promotion_usage']
    );

    $user = $this->createUser();

    $this->order = Order::create([
      'type' => 'default',
      'state' => 'completed',
      'mail' => 'test@example.com',
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
      'store_id' => $this->store,
      'uid' => $user->id(),
      'order_items' => [],
    ]);

    $order_item = OrderItem::create([
      'type' => 'test',
      'quantity' => 3,
      'unit_price' => [
        'number' => '10.00',
        'currency_code' => 'USD',
      ],
    ]);
    $order_item->save();
    $this->order->addItem($order_item);

    $this->profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'HR',
        'postal_code' => '31000',
        'locality' => 'Osijek',
        'address_line1' => 'J.J. Strossmayera 341',
        'administrative_area' => 'Osijek-baranja county',
        'given_name' => 'Drupal',
        'family_name' => 'Developer',
      ],
      'uid' => $user->id(),
    ]);

    $this->order->setBillingProfile($this->profile);
    $this->order->save();

    $this->zones = Zone::loadMultiple();
    foreach ($this->zones as $zone) {
      $zone_id = $zone->id();
      // Match by region.
      $this->promotions[$zone_id] = Promotion::create([
        'name' => sprintf('Promotion %s', $zone->label()),
        'order_types' => [$this->order->bundle()],
        'stores' => [$this->store->id()],
        'status' => TRUE,
        'offer' => [
          'target_plugin_id' => 'order_percentage_off',
          'target_plugin_configuration' => [
            'percentage' => '0.10',
          ],
        ],
        'conditions' => [
          [
            'target_plugin_id' => 'billing_address_zone',
            'target_plugin_configuration' => [
              'negate' => $zone_id === 'eea',
              'zones' => [
                $zone_id,
              ],
            ],
          ],
        ],
        'condition_operator' => 'AND',
      ]);
    }

  }

  /**
   * Verify profile and test zones data.
   */
  public function testZoneVerifyDataSet() {
    foreach ($this->zones as $zone) {
      assert($zone instanceof Zone);
      $territories = $zone->getZone()->getTerritories();
      $countries = [];
      foreach ($territories as $territory) {
        $countries[] = $territory->getCountryCode();
        if ($zone->id() === 'osijek_county') {
          $this->assertNotEmpty($territory->getIncludedPostalCodes());
        }
      }

      if (in_array($zone->id(), $this->nonHrZone, TRUE)) {
        $this->assertNotContains('HR', $countries);
      }
      else {
        $this->assertContains('HR', $countries);
      }
    }

    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());
    $this->assertEquals('31000', $address->getPostalCode());
  }

  /**
   * Tests match condition/zone based on country.
   */
  public function testZoneMatchEuCountryConditions() {
    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());

    // Condition match.
    $result = $this->promotions['eu']->applies($this->order);
    $this->assertTrue($result);

    // Zone match.
    $zone = Zone::load('eu');
    assert($zone instanceof ZoneInterface);
    $this->assertTrue($zone->match($address));
  }

  /**
   * Tests match condition/zone based on postal code.
   */
  public function testZoneMatchPostalConditions() {
    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());
    $this->assertEquals('31000', $address->getPostalCode());

    // Condition match.
    $result = $this->promotions['osijek_county']->applies($this->order);
    $this->assertTrue($result);

    // Zone match.
    $zone = Zone::load('osijek_county');
    assert($zone instanceof ZoneInterface);
    $this->assertTrue($zone->match($address));
  }

  /**
   * Tests negate condition and match zone based on country.
   */
  public function testZoneNegateConditions() {
    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());

    // Condition does not match whilst being negated.
    $result = $this->promotions['eea']->applies($this->order);
    $this->assertFalse($result);

    // Zone still match.
    $zone = Zone::load('eea');
    assert($zone instanceof ZoneInterface);
    $this->assertTrue($zone->match($address));
  }

  /**
   * Tests no match condition/zone based on country.
   */
  public function testZoneNoMatchConditions() {
    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());

    // Condition does not match whilst being negated.
    $result = $this->promotions['safta']->applies($this->order);
    $this->assertFalse($result);

    // Zone does not match.
    $zone = Zone::load('safta');
    assert($zone instanceof ZoneInterface);
    $this->assertFalse($zone->match($address));
  }

  /**
   * Tests all zones entities on commerce conditions.
   */
  public function testZoneMatchesConditions() {
    foreach ($this->promotions as $zone_id => $promotion) {
      $result = $promotion->applies($this->order);
      if (in_array($zone_id, $this->nonHrZone, TRUE)) {
        $this->assertFalse($result);
      }
      else {
        // EEA promotion have negated condition and should failed.
        if ($zone_id === 'eea') {
          $this->assertFalse($result);
        }
        else {
          $this->assertTRUE($result);
        }
      }
    }
  }

  /**
   * Tests all zones entities on zone match validation.
   */
  public function testZoneMatchesZones() {
    $address = $this->profile->get('address')->first();
    assert($address instanceof AddressItem);

    $this->assertEquals('HR', $address->getCountryCode());
    $this->assertEquals('31000', $address->getPostalCode());

    foreach ($this->zones as $zone_id => $zone) {
      $match = $zone->match($address);
      if (in_array($zone_id, $this->nonHrZone, TRUE)) {
        $this->assertFalse($match);

      }
      else {
        $this->assertTRUE($match);
      }
    }
  }

}
